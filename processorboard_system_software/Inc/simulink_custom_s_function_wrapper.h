#include "model.h"

#include "FreeRTOS.h"
//#include "adc.h"
#include "can.h"
#include "tim.h"
#include "gpio.h"

#define MAX_CAN_RX_COUNT 500



//typedef struct can_rx_queue_data{
//	int32_T id;
//	int32_T dlc;
//	uint8_T data_0;
//	uint8_T data_1;
//	uint8_T data_2;
//	uint8_T data_3;
//	uint8_T data_4;
//	uint8_T data_5;
//	uint8_T data_6;
//	uint8_T data_7;
//}can_rx_queue_data;

typedef struct can_rx_model_data{
	uint8_T data_0;
	uint8_T data_1;
	uint8_T data_2;
	uint8_T data_3;
	uint8_T data_4;
	uint8_T data_5;
	uint8_T data_6;
	uint8_T data_7;
	boolean_T received;
}can_rx_model_data;

extern can_rx_model_data* can_rx_model_data_array;

void init_drs();

void encoder_auslesen();


//ETHERNET DECLARATIONS

//extern struct netif gnetif;

//// ethernet verbindung tx
//struct udp_pcb *upcb_tx1;
//#define UDP_PORT_TX1 27030
//uint8_t IP_ADDRESS_TX1[4] = {192,168,178,20};
//struct ip_addr ipaddr_pc;

//// ethernet verbindung rx
//struct udp_pcb *upcb_rx1;
//#define UDP_PORT 27030
//uint8_t IP_ADDRESS_RX1[4] = {192,168,178,20};
//struct ip_addr ipaddr_pc;

