/*
 * This file should conatain a implementation for every external function in model.c Usually Outputs_wrapper and Update_wrapper for every S Function
 * current Version of Simulink Blocks:
 * gpio_in			 -
 * gpio_out			 -
 * adc_average	 v1.1
 * dac1					 -
 * can_rx				 v1.1
 * can_tx				 v1.1
 */
#include "simulink_custom_s_function_wrapper.h"
#include <stdlib.h>

#define ADC_SAMPLECOUNT 50

uint16_t samplearray[50];


/*struct samplearray{
  int samplecount;
  int channelcount;
  uint32_t* SampledValues;
}samplearray;*/

//struct samplearray adc1_samplearray;
//struct samplearray adc2_samplearray;
//struct samplearray adc3_samplearray;

CAN_FilterConfTypeDef  sFilterConfig1;
CAN_FilterConfTypeDef  sFilterConfig2;


uint32_T can_rx_ids_channel1[MAX_CAN_RX_COUNT];
uint32_T can_rx_ids_channel2[MAX_CAN_RX_COUNT];

int pwmFrequency = 0; //pwmFrequency in kHz
int act_position = 0;
int direction = 0;

int can_tx_block_count=0;
int can1_rx_block_count=0;
int can2_rx_block_count=0;
int adc_average_block_count = 0;
int basic_pwm_block_count = 0;
int drs_actual_position_block_count = 0;


static CanTxMsgTypeDef TxMessage1;
static CanRxMsgTypeDef RxMessage1;
static CanTxMsgTypeDef TxMessage2;
static CanRxMsgTypeDef RxMessage2;

can_rx_model_data* can_rx_model_data_array_channel1;
can_rx_model_data* can_rx_model_data_array_channel2;


// If an s-function needs initialisation the initialisation function is beeing called from the Outputs_wrapper when the model is beeing run the first time.
// After the first run the custom_s_functions_initialised variable is beeing set to true.
boolean_T custom_s_functions_initialized = false;


// Is beeing called after every initialization function has been called
void after_initialization_step(){
	//ADC after_initializeation_step
	if(adc_average_block_count>0){
		/*adc1_samplearray.SampledValues = (uint32_t*) malloc(hadc1.Init.NbrOfConversion*ADC_SAMPLECOUNT*sizeof(uint32_t)*3);
		adc1_samplearray.samplecount = ADC_SAMPLECOUNT;
		adc1_samplearray.channelcount = hadc1.Init.NbrOfConversion;
		HAL_ADC_Start_DMA(&hadc1, adc1_samplearray.SampledValues, adc1_samplearray.channelcount*adc1_samplearray.samplecount);
*/
		
		// Achtung!!! L�nge ist Anzahl der Werte in etsprechender CubeMX Konfiguration.
		// Hier also 50(ADC_SAMPLECOUNT) Werte vom Typ Half Word und damit 100 Byte (ADC_SAMPLECOUNT*2).
		//HAL_ADC_Start_DMA(&hadc1, (uint32_t *)samplearray, ADC_SAMPLECOUNT);

		/*adc3_samplearray.SampledValues = (uint32_t*) malloc(hadc3.Init.NbrOfConversion*ADC_SAMPLECOUNT*sizeof(uint32_t));
		adc3_samplearray.samplecount = ADC_SAMPLECOUNT;
		adc3_samplearray.channelcount = hadc3.Init.NbrOfConversion;
		HAL_ADC_Start_DMA(&hadc3, adc3_samplearray.SampledValues, adc3_samplearray.channelcount*adc3_samplearray.samplecount);
*/
	}
	//CAN after_initializeation_step
	if(can_tx_block_count>0 || can1_rx_block_count+can2_rx_block_count>0){

		//filter can1
		sFilterConfig1.FilterNumber = 0;
		sFilterConfig1.FilterMode = CAN_FILTERMODE_IDMASK;
		sFilterConfig1.FilterScale = CAN_FILTERSCALE_16BIT;
		sFilterConfig1.FilterIdHigh = 0x0000;
		sFilterConfig1.FilterIdLow = 0x0000;
		sFilterConfig1.FilterMaskIdHigh = 0x0000;
		sFilterConfig1.FilterMaskIdLow = 0x0000;
		sFilterConfig1.FilterFIFOAssignment = CAN_FILTER_FIFO0;
		sFilterConfig1.FilterActivation = ENABLE;
		sFilterConfig1.BankNumber = 14;
		HAL_CAN_ConfigFilter(&hcan1, &sFilterConfig1);
		//filter can2
		sFilterConfig2.FilterNumber = 14;
		sFilterConfig2.FilterMode = CAN_FILTERMODE_IDMASK;
		sFilterConfig2.FilterScale = CAN_FILTERSCALE_16BIT;
		sFilterConfig2.FilterIdHigh = 0x0000;
		sFilterConfig2.FilterIdLow = 0x0000;
		sFilterConfig2.FilterMaskIdHigh = 0x0000;
		sFilterConfig2.FilterMaskIdLow = 0x0000;
		sFilterConfig2.FilterFIFOAssignment = CAN_FILTER_FIFO1;
		sFilterConfig2.FilterActivation = ENABLE;
		sFilterConfig2.BankNumber = 14;
		//HAL_CAN_ConfigFilter(&hcan2, &sFilterConfig2);

		//setup can massages
		hcan1.pTxMsg = &TxMessage1;
		hcan1.pRxMsg = &RxMessage1;
		//hcan2.pTxMsg = &TxMessage2;
		//hcan2.pRxMsg = &RxMessage2;

		//can tranceiver standby
		HAL_GPIO_WritePin(GPIOG,GPIO_PIN_9,0);//can1
		HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,0);//can2



		can_rx_model_data_array_channel1=malloc(sizeof(can_rx_model_data)*can1_rx_block_count);
		can_rx_model_data_array_channel2=malloc(sizeof(can_rx_model_data)*can2_rx_block_count);
		for(int i=0;i<can1_rx_block_count;i++){
			can_rx_model_data_array_channel1[i].received=false;
		}
		for(int i=0;i<can2_rx_block_count;i++){
			can_rx_model_data_array_channel2[i].received=false;
		}

		//start receive
		volatile HAL_StatusTypeDef res;
		res = HAL_CAN_Receive_IT(&hcan1,CAN_FIFO0);
		//res = HAL_CAN_Receive_IT(&hcan2,CAN_FIFO1);
	}

	if(basic_pwm_block_count > 0){
		htim2.Instance = TIM2;
		htim2.Init.Prescaler = 84-1; //init Prescaler according to APB1 frequency now: 1.000.000 Hz
		htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
		if(pwmFrequency > 10 ){
			htim2.Init.Period = (10 * 10) - 1;
			
		}else {
			if(pwmFrequency < 1){
				htim2.Init.Period = (1 * 10) - 1;	
			}else{
				htim2.Init.Period = (pwmFrequency * 10) - 1;
			}
		}
		
		htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
		if (HAL_TIM_Base_Init(&htim2) != HAL_OK)
		{
    _Error_Handler(__FILE__, __LINE__);
		}
		
		HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4); 
		
	}

	if(drs_actual_position_block_count > 0){
		init_drs();
		HAL_TIM_Base_Start_IT(&htim3); 
		act_position = 0;
		
	}
}

//GPIO IN

void gpio_in_Outputs_wrapper(const int32_T *port_popupvalue, const int32_T *pin_number, boolean_T *gpio_in, const real_T *xD){
	if(*port_popupvalue==1){
		*gpio_in=HAL_GPIO_ReadPin(GPIOA,(1<<*pin_number));
	}else if (*port_popupvalue==2){
		*gpio_in=HAL_GPIO_ReadPin(GPIOB,(1<<*pin_number));
	}else if (*port_popupvalue==3){
		*gpio_in=HAL_GPIO_ReadPin(GPIOC,(1<<*pin_number));
	}else if (*port_popupvalue==4){
		*gpio_in=HAL_GPIO_ReadPin(GPIOD,(1<<*pin_number));
	}else if (*port_popupvalue==5){
		*gpio_in=HAL_GPIO_ReadPin(GPIOE,(1<<*pin_number));
	}else if (*port_popupvalue==6){
		*gpio_in=HAL_GPIO_ReadPin(GPIOF,(1<<*pin_number));
	}else if (*port_popupvalue==7){
		*gpio_in=HAL_GPIO_ReadPin(GPIOG,(1<<*pin_number));
	}else if (*port_popupvalue==8){
		*gpio_in=HAL_GPIO_ReadPin(GPIOH,(1<<*pin_number));
	}else if (*port_popupvalue==9){
		*gpio_in=HAL_GPIO_ReadPin(GPIOI,(1<<*pin_number));
	}
}

void gpio_in_Update_wrapper(const int32_T *port_popupvalue,
                            const int32_T *pin_number,
                            boolean_T *gpio_in,
                            real_T *xD) {
	// No content
}

//GPIO OUT

void gpio_out_Outputs_wrapper(const int32_T *port_popupvalue, const int32_T *pin_number, const boolean_T *gpio_out, const real_T *xD){
	if(*port_popupvalue==1){
		HAL_GPIO_WritePin(GPIOA,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==2){
		HAL_GPIO_WritePin(GPIOB,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==3){
		HAL_GPIO_WritePin(GPIOC,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==4){
		HAL_GPIO_WritePin(GPIOD,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==5){
		HAL_GPIO_WritePin(GPIOE,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==6){
		HAL_GPIO_WritePin(GPIOF,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==7){
		HAL_GPIO_WritePin(GPIOG,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==8){
		HAL_GPIO_WritePin(GPIOH,(1<<*pin_number),*gpio_out);
	}else if (*port_popupvalue==9){
		HAL_GPIO_WritePin(GPIOI,(1<<*pin_number),*gpio_out);
	}
}

void gpio_out_Update_wrapper(const int32_T *port_popupvalue,
                             const int32_T *pin_number,
                             const boolean_T *gpio_out,
                             real_T *xD) {
	// No content
}

//ADC
//uint32_t get_ADC_Value(struct samplearray adc_samplearray, int channel_ranknumber){
// int i;
// uint32_t r = adc_samplearray.SampledValues[i];
// for(i = channel_ranknumber-1; i<adc_samplearray.channelcount*adc_samplearray.samplecount; i += adc_samplearray.channelcount){
//	r = (r + adc_samplearray.SampledValues[i]);
// }
// return r/adc_samplearray.samplecount;
//}

void adc_average_Initialize(){
	adc_average_block_count++;
}
void adc_average_Outputs_wrapper(const int32_T *rank,const int32_T *adc,uint32_T *adc_value,const real_T *xD){
	
	if(!custom_s_functions_initialized){
		adc_average_Initialize();
		return;
	}
//	if(*adc == 1){
//		*adc_value=get_ADC_Value(adc1_samplearray, *rank);
//	}
	/*if(*adc == 2){
		uint32_t value = 0; 
		for(unsigned int i = 0; i < ADC_SAMPLECOUNT; i+=2){
			value += samplearray[i];		
		}
		value /= (ADC_SAMPLECOUNT/2);
		*adc_value = value;
	}*/
//	if(*adc == 3){
//		*adc_value=get_ADC_Value(adc3_samplearray, *rank);
//	}

}
void adc_average_Update_wrapper(const int32_T *rank,const int32_T *adc,const uint32_T *adc_value,real_T *xD){
}

//DAC

void dac1_Initialize(){
//	HAL_DAC_Start(&hdac, DAC_CHANNEL_1);
}
void dac1_Outputs_wrapper(const uint32_T *dac_value,const real_T *xD){
	if(!custom_s_functions_initialized){
		dac1_Initialize();
	}
//	HAL_DAC_SetValue(&hdac, DAC_CHANNEL_1, DAC_ALIGN_12B_R, *dac_value);
}
void dac1_Update_wrapper(const uint32_T *dac_value,real_T *xD){
}

// CAN Tx

void can_tx_Initialize(){
	can_tx_block_count++;
}

void can_tx_Outputs_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD){
	if(!custom_s_functions_initialized){
		can_tx_Initialize();
	}
	if(*trigger && custom_s_functions_initialized){
		if(*channel==1){
		hcan1.pTxMsg->StdId = *id;
		hcan1.pTxMsg->RTR = CAN_RTR_DATA;
		hcan1.pTxMsg->IDE = CAN_ID_STD;
		hcan1.pTxMsg->DLC = *dlc;
		hcan1.pTxMsg->Data[0] = data[0];
		hcan1.pTxMsg->Data[1] = data[1];
		hcan1.pTxMsg->Data[2] = data[2];
		hcan1.pTxMsg->Data[3] = data[3];
		hcan1.pTxMsg->Data[4] = data[4];
		hcan1.pTxMsg->Data[5] = data[5];
		hcan1.pTxMsg->Data[6] = data[6];
		hcan1.pTxMsg->Data[7] = data[7];
		//volatile HAL_StatusTypeDef res =
		volatile HAL_StatusTypeDef res = HAL_CAN_Transmit(&hcan1,1);
		}
		if(*channel==2){
		/*hcan2.pTxMsg->StdId = *id;
		hcan2.pTxMsg->RTR = CAN_RTR_DATA;
		hcan2.pTxMsg->IDE = CAN_ID_STD;
		hcan2.pTxMsg->DLC = *dlc;
		hcan2.pTxMsg->Data[0] = data[0];
		hcan2.pTxMsg->Data[1] = data[1];
		hcan2.pTxMsg->Data[2] = data[2];
		hcan2.pTxMsg->Data[3] = data[3];
		hcan2.pTxMsg->Data[4] = data[4];
		hcan2.pTxMsg->Data[5] = data[5];
		hcan2.pTxMsg->Data[6] = data[6];
		hcan2.pTxMsg->Data[7] = data[7];
		//volatile HAL_StatusTypeDef res =
		volatile HAL_StatusTypeDef res = HAL_CAN_Transmit(&hcan2,1);*/
		}
	}

}


void can_tx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD){
}

//CAN Rx

void can_rx_Initialize(const uint32_T *id, const uint32_T *channel){
	if(can1_rx_block_count+can2_rx_block_count<MAX_CAN_RX_COUNT){
		if(*channel == 1){
			can_rx_ids_channel1[can1_rx_block_count]=*id;
			can1_rx_block_count++;
		}
		if(*channel == 2){
			can_rx_ids_channel2[can2_rx_block_count]=*id;
			can2_rx_block_count++;
		}
	}
}

void can_rx_Outputs_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  const real_T *xD){
	if(!custom_s_functions_initialized){
		can_rx_Initialize(id,channel);
	}
	else
	{
		if(*channel==1){
			unsigned int index;
			//find the id in the can_rx_ids array	with a linear search		
			for (index = 0; can_rx_ids_channel1[index] != *id; index += 1);

			data[0] = can_rx_model_data_array_channel1[index].data_0;
			data[1] = can_rx_model_data_array_channel1[index].data_1;
			data[2] = can_rx_model_data_array_channel1[index].data_2;
			data[3] = can_rx_model_data_array_channel1[index].data_3;
			data[4] = can_rx_model_data_array_channel1[index].data_4;
			data[5] = can_rx_model_data_array_channel1[index].data_5;
			data[6] = can_rx_model_data_array_channel1[index].data_6;
			data[7] = can_rx_model_data_array_channel1[index].data_7;

			*received = can_rx_model_data_array_channel1[index].received;

			can_rx_model_data_array_channel1[index].received = false;
		}
		if(*channel==2){
			int index = 0;
			//find the id in the can_rx_ids array
			while(can_rx_ids_channel2[index] != *id){
				index++;
			}

			data[0] = can_rx_model_data_array_channel2[index].data_0;
			data[1] = can_rx_model_data_array_channel2[index].data_1;
			data[2] = can_rx_model_data_array_channel2[index].data_2;
			data[3] = can_rx_model_data_array_channel2[index].data_3;
			data[4] = can_rx_model_data_array_channel2[index].data_4;
			data[5] = can_rx_model_data_array_channel2[index].data_5;
			data[6] = can_rx_model_data_array_channel2[index].data_6;
			data[7] = can_rx_model_data_array_channel2[index].data_7;

			*received = can_rx_model_data_array_channel2[index].received;

			can_rx_model_data_array_channel2[index].received = false;
		}
	}
}

void can_rx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  const real_T *xD){

}

void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef * hcan){
//	can_rx_queue_data queue_data_struct;
//	queue_data_struct.id = hcan->pRxMsg->StdId;
//	queue_data_struct.dlc = hcan->pRxMsg->DLC;
//	queue_data_struct.data_0 = hcan->pRxMsg->Data[0];
//	queue_data_struct.data_1 = hcan->pRxMsg->Data[1];
//	queue_data_struct.data_2 = hcan->pRxMsg->Data[2];
//	queue_data_struct.data_3 = hcan->pRxMsg->Data[3];
//	queue_data_struct.data_4 = hcan->pRxMsg->Data[4];
//	queue_data_struct.data_5 = hcan->pRxMsg->Data[5];
//	queue_data_struct.data_6 = hcan->pRxMsg->Data[6];
//	queue_data_struct.data_7 = hcan->pRxMsg->Data[7];
//	xQueueSendFromISR(can_rx_queueHandle, &queue_data_struct, NULL);
//	HAL_CAN_Receive_IT(&hcan1,CAN_FIFO0);
	if(custom_s_functions_initialized){

		if(hcan == &hcan1){
			for(int index=0;index<can1_rx_block_count;index++){
				//check if the recieved massage has an entry in the can_rx_ids array
				if (hcan->pRxMsg->StdId == can_rx_ids_channel1[index]){
					//write the data of the massage into the can_rx_data_array
					can_rx_model_data_array_channel1[index].data_0 = hcan->pRxMsg->Data[0];
					can_rx_model_data_array_channel1[index].data_1 = hcan->pRxMsg->Data[1];
					can_rx_model_data_array_channel1[index].data_2 = hcan->pRxMsg->Data[2];
					can_rx_model_data_array_channel1[index].data_3 = hcan->pRxMsg->Data[3];
					can_rx_model_data_array_channel1[index].data_4 = hcan->pRxMsg->Data[4];
					can_rx_model_data_array_channel1[index].data_5 = hcan->pRxMsg->Data[5];
					can_rx_model_data_array_channel1[index].data_6 = hcan->pRxMsg->Data[6];
					can_rx_model_data_array_channel1[index].data_7 = hcan->pRxMsg->Data[7];
					can_rx_model_data_array_channel1[index].received = true;
					
					break;
				}
			}
		}
		/*if(hcan == &hcan2){
			for(int index=0;index<can2_rx_block_count;index++){
				//check if the recieved massage has an entry in the can_rx_ids array
				if (hcan->pRxMsg->StdId == can_rx_ids_channel2[index]){
					//write the data of the massage into the can_rx_data_array
					can_rx_model_data_array_channel2[index].data_0 = hcan->pRxMsg->Data[0];
					can_rx_model_data_array_channel2[index].data_1 = hcan->pRxMsg->Data[1];
					can_rx_model_data_array_channel2[index].data_2 = hcan->pRxMsg->Data[2];
					can_rx_model_data_array_channel2[index].data_3 = hcan->pRxMsg->Data[3];
					can_rx_model_data_array_channel2[index].data_4 = hcan->pRxMsg->Data[4];
					can_rx_model_data_array_channel2[index].data_5 = hcan->pRxMsg->Data[5];
					can_rx_model_data_array_channel2[index].data_6 = hcan->pRxMsg->Data[6];
					can_rx_model_data_array_channel2[index].data_7 = hcan->pRxMsg->Data[7];
					can_rx_model_data_array_channel2[index].received = true;
					break;
				}
			}
		}*/

	}

	//force Receive_IT without messing with HAL_CAN_StateTypeDef

	/* Enable Error warning Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_EWG);

	/* Enable Error passive Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_EPV);

	/* Enable Bus-off Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_BOF);

	/* Enable Last error code Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_LEC);

	/* Enable Error Interrupt */
	__HAL_CAN_ENABLE_IT(hcan, CAN_IT_ERR);

	/* Process unlocked */
	__HAL_UNLOCK(hcan);


    if(hcan == &hcan1)//can1 frames to fifo0
    {
      /* Enable FIFO 0 message pending Interrupt */
      __HAL_CAN_ENABLE_IT(hcan, CAN_IT_FMP0);
    }
/*    if(hcan == &hcan2)//can2 frames to fifo0
    {
      /* Enable FIFO 1 message pending Interrupt */
/*      __HAL_CAN_ENABLE_IT(hcan, CAN_IT_FMP1);
    }*/

}



// Input capture timer for flow-meters
// Counts between two capture events that correlates with the input frequency
uint32_T u32_CH3_Gap = 0;
uint32_T u32_CH4_Gap = 0;
// A counter wrap occurred between two capture events
boolean_T b_CH3_PeriodBeforeCapture = false;
boolean_T b_CH4_PeriodBeforeCapture = false;
// A double counter wrap occurred and the channel is defined as overflowed
boolean_T b_CH3_Overflow = false;
boolean_T b_CH4_Overflow = false;

// S-function
void Capture_Timer_Outputs_wrapper(const uint8_T *CH, uint32_T *Gap) {
	// Switch channel
	// Output gap value
	if(3 == *CH)
		*Gap = u32_CH3_Gap;
	else if(4 == *CH)
		*Gap = u32_CH4_Gap;
}

void Capture_Timer_Update_wrapper(const uint8_T *CH, uint32_T *Gap, real_T *xD) {
	// No content
}

// Callbacks
// HAL_TIM_PeriodElapsedCallback is defined in main.c

void HAL_TIM_IC_CaptureCallback (TIM_HandleTypeDef * htim) {
	// The previous capture value
	static uint32_T u32_CH3_LastCapture = 0;
	static uint32_T u32_CH4_LastCapture = 0;
  // Switch timer instance
/*  if(htim == &htim4) {
  	if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_3) {
  		// Capture event on channel 3
  		// Calculate gap value
  		if(b_CH3_Overflow == true) {
  			// No valid last capture value available due to channel overflow
  			// Start new cycle by waiting for a second capture event with valid last capture value
  			b_CH3_Overflow = false;
  			b_CH3_PeriodBeforeCapture = false;
  			u32_CH3_LastCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
  			// Gap value has already been set to 0 in HAL_TIM_PeriodElapsedCallback()
  		}	else if(b_CH3_PeriodBeforeCapture == true) {
  			// Consider counter wrap
  			u32_CH3_Gap = (0xFFFFFFFF - u32_CH3_LastCapture) + HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3);
  			b_CH3_PeriodBeforeCapture = false;
			}	else u32_CH3_Gap = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_3) - u32_CH3_LastCapture;
  	} else if(htim->Channel == HAL_TIM_ACTIVE_CHANNEL_4) {
  		// Capture event on channel 4
  		// Calculate gap value
  		if(b_CH4_Overflow == true) {
  			// No valid last capture value available due to channel overflow
  			// Start new cycle by waiting for a second capture event with valid last capture value
  			b_CH4_Overflow = false;
  			b_CH4_PeriodBeforeCapture = false;
  			u32_CH4_LastCapture = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
  		} else if(b_CH4_PeriodBeforeCapture == true) {
  			// Consider counter wrap
  			u32_CH4_Gap = (0xFFFFFFFF - u32_CH4_LastCapture) + HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4);
  			b_CH4_PeriodBeforeCapture = false;
			}	else u32_CH4_Gap = HAL_TIM_ReadCapturedValue(htim, TIM_CHANNEL_4) - u32_CH4_LastCapture;
  	}
  }*/
}

void basic_pwm_Outputs_wrapper(const real_T *dutyCylce,
			const uint32_T *frequency,
			const real_T *xD)
{
	if(!custom_s_functions_initialized){
			pwmFrequency = *frequency;
			basic_pwm_block_count++;
	}
	int period = (pwmFrequency * 10) -1;
	double pulse = period * (*dutyCylce);
	
	if(pulse > period){
		pulse = period;
	}
	if(pulse < 0){
		pulse = 0;
	}
	
	TIM_OC_InitTypeDef sConfigOC;  
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = (int)pulse;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
	
  if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  HAL_TIM_MspPostInit(&htim2);
	
	HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_4); 

}

void basic_pwm_Update_wrapper(const real_T *dutyCylce,
			const uint32_T *frequency,
			real_T *xD)
{}

void init_drs(){
		/*while(HAL_GPIO_ReadPin(GPIOB, endSwitch1_Pin)){
				HAL_GPIO_WritePin(GPIOB, INA_Pin, GPIO_PIN_SET);
				HAL_GPIO_WritePin(GPIOA, INB_Pin, GPIO_PIN_RESET);

		}*/
}


void drs_actual_position_Outputs_wrapper(int32_T *actual_position,
			const real_T *xD)
{
	if(!custom_s_functions_initialized){
			drs_actual_position_block_count++;
	}

	*actual_position = act_position;

}


void drs_actual_position_Update_wrapper(int32_T *actual_position,
			real_T *xD)
{}
	
/* Set interrupt handlers */
/* Handle PB14 interrupt */
/*void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin){
		switch(GPIO_Pin){
			case GPIO_PIN_14:
					//detect rising edge
					if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)){
						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)){
							direction = -1;
						} else {
							direction = 1;
						}
					} 
					//falling edge
					else {
						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)){
							direction = 1;
						} else {
							direction = -1;
						}
					}
					
					act_position += direction;
					//HAL_GPIO_TogglePin(GPIOC, LED_USER2_Pin);
				break;
			case GPIO_PIN_15:			
					//detect rising edge		
					if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15)){
						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)){
							direction = 1;
						} else {
							direction = -1;
						}
					} 
					//detect falling edge		
					else {
						if(HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)){
							direction = -1;
						} else {
							direction = 1;
						}
					}
					act_position += direction;
					//HAL_GPIO_TogglePin(GPIOC, LED_USER1_Pin);
				break;
		}
}*/

void encoder_auslesen(){
	int table [4][4] = {{0,1,-1,0}, {-1,0,0,1}, {1,0,0,-1}, {0,-1,1,0}};
	static int last_quadrature_value0 = 0;
	volatile int quadrature_input0 = 2*HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_14)+HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15);
	int new_quadrature_value0 = quadrature_input0;
	act_position += table[last_quadrature_value0][new_quadrature_value0];
	last_quadrature_value0 = new_quadrature_value0;
}
