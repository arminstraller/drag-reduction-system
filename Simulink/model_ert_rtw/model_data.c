/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model_data.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.271
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 08 04:40:19 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Invariant block signals (auto storage) */
const ConstB rtConstB = {
  7,                                   /* '<S8>/Subtract' */
  1,                                   /* '<S20>/Subtract' */
  10,                                  /* '<S7>/Subtract' */
  4,                                   /* '<S21>/Subtract' */
  7,                                   /* '<S23>/Subtract' */
  4,                                   /* '<S24>/Subtract' */
  5,                                   /* '<S25>/Subtract' */
  0,                                   /* '<S9>/Subtract' */
  3,                                   /* '<S11>/Subtract' */
  6,                                   /* '<S13>/Subtract' */
  5,                                   /* '<S14>/Subtract' */
  1                                    /* '<S10>/Subtract' */
};

/* Constant parameters (auto storage) */
const ConstP rtConstP = {
  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant2'
   *   '<S8>/Constant'
   *   '<S8>/Constant2'
   *   '<S9>/Constant1'
   *   '<S9>/Constant2'
   *   '<S10>/Constant2'
   *   '<S11>/Constant'
   *   '<S11>/Constant2'
   *   '<S13>/Constant'
   *   '<S13>/Constant2'
   *   '<S14>/Constant'
   *   '<S14>/Constant2'
   *   '<S20>/Constant2'
   *   '<S21>/Constant'
   *   '<S21>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   *   '<S24>/Constant2'
   *   '<S25>/Constant2'
   */
  1,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S10>/Constant1'
   *   '<S20>/Constant'
   *   '<S20>/Constant1'
   */
  2,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S9>/Constant'
   *   '<S10>/Constant'
   *   '<S24>/Constant'
   *   '<S25>/Constant'
   */
  3,

  /* Computed Parameter: Constant_Value_pr
   * Referenced by: '<S3>/Constant'
   */
  392U,

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant1'
   *   '<S3>/Constant1'
   *   '<S3>/Constant2'
   */
  1U,

  /* Computed Parameter: Constant_Value_g
   * Referenced by: '<S1>/Constant'
   */
  400U,

  /* Computed Parameter: Constant_Value_n3
   * Referenced by: '<S22>/Constant'
   */
  10U,

  /* Pooled Parameter (Expression: true)
   * Referenced by:
   *   '<Root>/Constant4'
   *   '<Root>/Constant5'
   *   '<Root>/Pulse Generator2'
   *   '<Root>/Pulse Generator3'
   */
  1,

  /* Pooled Parameter (Expression: false)
   * Referenced by:
   *   '<Root>/Constant1'
   *   '<Root>/Constant6'
   */
  0
};

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
