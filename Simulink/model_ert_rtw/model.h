/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.h
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.271
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 08 04:40:19 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#ifndef RTW_HEADER_model_h_
#define RTW_HEADER_model_h_
#include <string.h>
#include <math.h>
#ifndef model_COMMON_INCLUDES_
# define model_COMMON_INCLUDES_
#include <string.h>
#include "rtwtypes.h"
#include "can_message.h"
#endif                                 /* model_COMMON_INCLUDES_ */

/* Macros for accessing real-time model data structure */
#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

/* Forward declaration for rtModel */
typedef struct tag_RTM RT_MODEL;

/* user code (top of export header file) */
#include "can_message.h"

/* Block signals and states (auto storage) for system '<Root>' */
typedef struct {
  CAN_DATATYPE CANPack6;               /* '<Root>/CAN Pack6' */
  CAN_DATATYPE CANPack2;               /* '<Root>/CAN Pack2' */
  real_T DiscreteTransferFcn_states[4];/* '<S17>/Discrete Transfer Fcn' */
  real_T CANUnpack2_o1;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o2;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o3;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o4;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o5;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o6;                /* '<Root>/CAN Unpack2' */
  real_T CANUnpack2_o7;                /* '<Root>/CAN Unpack2' */
  real_T Saturation;                   /* '<S6>/Saturation' */
  real_T posSollOUT;                   /* '<Root>/Chart' */
  real_T speed;                        /* '<Root>/Chart' */
  real_T SFunctionBuilder_DSTATE;      /* '<S8>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_m;    /* '<S26>/S-Function Builder' */
  real_T can_tx_DSTATE;                /* '<S3>/can_tx' */
  real_T Integrator_DSTATE;            /* '<S12>/Integrator' */
  real_T SFunctionBuilder_DSTATE_j;    /* '<S7>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_f;    /* '<S1>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_a;    /* '<S20>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_e;    /* '<S21>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_i;    /* '<S23>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_mz;   /* '<S22>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_i3;   /* '<S24>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_mi;   /* '<S25>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_n;    /* '<S9>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_g;    /* '<S11>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_l;    /* '<S13>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_ih;   /* '<S14>/S-Function Builder' */
  real_T SFunctionBuilder_DSTATE_gi;   /* '<S10>/S-Function Builder' */
  real_T posNull;                      /* '<Root>/Data Store Memory' */
  int32_T SFunctionBuilder;            /* '<S26>/S-Function Builder' */
  int32_T clockTickCounter;            /* '<Root>/Pulse Generator3' */
  int32_T clockTickCounter_j;          /* '<Root>/Pulse Generator2' */
  int_T CANPack6_ModeSignalID;         /* '<Root>/CAN Pack6' */
  int_T CANUnpack6_ModeSignalID;       /* '<Root>/CAN Unpack6' */
  int_T CANUnpack6_StatusPortID;       /* '<Root>/CAN Unpack6' */
  int_T CANUnpack2_ModeSignalID;       /* '<Root>/CAN Unpack2' */
  int_T CANUnpack2_StatusPortID;       /* '<Root>/CAN Unpack2' */
  uint16_T temporalCounter_i1;         /* '<Root>/Chart' */
  uint8_T MatrixConcatenate1[8];       /* '<Root>/Matrix Concatenate1' */
  uint8_T SFunctionBuilder_o2[8];      /* '<S1>/S-Function Builder' */
  uint8_T is_active_c3_model;          /* '<Root>/Chart' */
  uint8_T is_c3_model;                 /* '<Root>/Chart' */
  boolean_T PulseGenerator3;           /* '<Root>/Pulse Generator3' */
  boolean_T SFunctionBuilder_f;        /* '<S8>/S-Function Builder' */
  boolean_T Compare;                   /* '<S5>/Compare' */
  boolean_T SFunctionBuilder_d;        /* '<S7>/S-Function Builder' */
  boolean_T SFunctionBuilder_o1;       /* '<S1>/S-Function Builder' */
  boolean_T Compare_k;                 /* '<S18>/Compare' */
  boolean_T Compare_h;                 /* '<S19>/Compare' */
  boolean_T SFunctionBuilder_a;        /* '<S23>/S-Function Builder' */
  boolean_T LogicalOperator2;          /* '<S6>/Logical Operator2' */
  boolean_T LogicalOperator3;          /* '<S6>/Logical Operator3' */
  boolean_T LogicalOperator;           /* '<Root>/Logical Operator' */
  boolean_T PulseGenerator2;           /* '<Root>/Pulse Generator2' */
} DW;

/* Invariant block signals (auto storage) */
typedef struct {
  const int32_T Subtract;              /* '<S8>/Subtract' */
  const int32_T Subtract_l;            /* '<S20>/Subtract' */
  const int32_T Subtract_m;            /* '<S7>/Subtract' */
  const int32_T Subtract_a;            /* '<S21>/Subtract' */
  const int32_T Subtract_mv;           /* '<S23>/Subtract' */
  const int32_T Subtract_i;            /* '<S24>/Subtract' */
  const int32_T Subtract_h;            /* '<S25>/Subtract' */
  const int32_T Subtract_f;            /* '<S9>/Subtract' */
  const int32_T Subtract_mr;           /* '<S11>/Subtract' */
  const int32_T Subtract_o;            /* '<S13>/Subtract' */
  const int32_T Subtract_fe;           /* '<S14>/Subtract' */
  const int32_T Subtract_g;            /* '<S10>/Subtract' */
} ConstB;

/* Constant parameters (auto storage) */
typedef struct {
  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant2'
   *   '<S8>/Constant'
   *   '<S8>/Constant2'
   *   '<S9>/Constant1'
   *   '<S9>/Constant2'
   *   '<S10>/Constant2'
   *   '<S11>/Constant'
   *   '<S11>/Constant2'
   *   '<S13>/Constant'
   *   '<S13>/Constant2'
   *   '<S14>/Constant'
   *   '<S14>/Constant2'
   *   '<S20>/Constant2'
   *   '<S21>/Constant'
   *   '<S21>/Constant2'
   *   '<S23>/Constant'
   *   '<S23>/Constant2'
   *   '<S24>/Constant2'
   *   '<S25>/Constant2'
   */
  int32_T pooled3;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S7>/Constant'
   *   '<S10>/Constant1'
   *   '<S20>/Constant'
   *   '<S20>/Constant1'
   */
  int32_T pooled5;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S9>/Constant'
   *   '<S10>/Constant'
   *   '<S24>/Constant'
   *   '<S25>/Constant'
   */
  int32_T pooled7;

  /* Computed Parameter: Constant_Value_pr
   * Referenced by: '<S3>/Constant'
   */
  uint32_T Constant_Value_pr;

  /* Pooled Parameter (Expression: )
   * Referenced by:
   *   '<S1>/Constant1'
   *   '<S3>/Constant1'
   *   '<S3>/Constant2'
   */
  uint32_T pooled9;

  /* Computed Parameter: Constant_Value_g
   * Referenced by: '<S1>/Constant'
   */
  uint32_T Constant_Value_g;

  /* Computed Parameter: Constant_Value_n3
   * Referenced by: '<S22>/Constant'
   */
  uint32_T Constant_Value_n3;

  /* Pooled Parameter (Expression: true)
   * Referenced by:
   *   '<Root>/Constant4'
   *   '<Root>/Constant5'
   *   '<Root>/Pulse Generator2'
   *   '<Root>/Pulse Generator3'
   */
  boolean_T pooled10;

  /* Pooled Parameter (Expression: false)
   * Referenced by:
   *   '<Root>/Constant1'
   *   '<Root>/Constant6'
   */
  boolean_T pooled11;
} ConstP;

/* Real-time Model Data Structure */
struct tag_RTM {
  const char_T * volatile errorStatus;
};

/* Block signals and states (auto storage) */
extern DW rtDW;
extern const ConstB rtConstB;          /* constant block i/o */

/* Constant parameters (auto storage) */
extern const ConstP rtConstP;

/* Model entry point functions */
extern void model_initialize(void);
extern void model_step(void);

/* Real-time Model object */
extern RT_MODEL *const rtM;

/*-
 * These blocks were eliminated from the model due to optimizations:
 *
 * Block '<S12>/Kb' : Eliminated nontunable gain of 1
 */

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'model'
 * '<S1>'   : 'model/CAN Rx1'
 * '<S2>'   : 'model/CAN Rx4'
 * '<S3>'   : 'model/CAN Tx2'
 * '<S4>'   : 'model/Chart'
 * '<S5>'   : 'model/Compare To Constant'
 * '<S6>'   : 'model/DC Motor Control'
 * '<S7>'   : 'model/End Switch DOWN '
 * '<S8>'   : 'model/GPIO in'
 * '<S9>'   : 'model/LED_error'
 * '<S10>'  : 'model/LED_heartbeat'
 * '<S11>'  : 'model/MS_EN'
 * '<S12>'  : 'model/PID Controller'
 * '<S13>'  : 'model/SEL0'
 * '<S14>'  : 'model/SEL1'
 * '<S15>'  : 'model/Sample and Hold'
 * '<S16>'  : 'model/Subsystem2'
 * '<S17>'  : 'model/Transfer Fcn2'
 * '<S18>'  : 'model/DC Motor Control/Compare To Constant'
 * '<S19>'  : 'model/DC Motor Control/Compare To Constant1'
 * '<S20>'  : 'model/DC Motor Control/INA'
 * '<S21>'  : 'model/DC Motor Control/INB'
 * '<S22>'  : 'model/DC Motor Control/Subsystem'
 * '<S23>'  : 'model/DC Motor Control/error'
 * '<S24>'  : 'model/DC Motor Control/user3'
 * '<S25>'  : 'model/DC Motor Control/user4'
 * '<S26>'  : 'model/Subsystem2/Subsystem1'
 */
#endif                                 /* RTW_HEADER_model_h_ */

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
