/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * File: model.c
 *
 * Code generated for Simulink model 'model'.
 *
 * Model version                  : 1.271
 * Simulink Coder version         : 8.11 (R2016b) 25-Aug-2016
 * C/C++ source code generated on : Fri Jun 08 04:40:19 2018
 *
 * Target selection: ert.tlc
 * Embedded hardware selection: ARM Compatible->ARM Cortex
 * Code generation objectives:
 *    1. Execution efficiency
 *    2. RAM efficiency
 * Validation result: Not run
 */

#include "model.h"

/* Named constants for Chart: '<Root>/Chart' */
#define IN_home                        ((uint8_T)1U)
#define IN_homing                      ((uint8_T)2U)
#define IN_operational                 ((uint8_T)3U)
#define IN_save                        ((uint8_T)4U)
#define IN_startup                     ((uint8_T)5U)

/* Block signals and states (auto storage) */
DW rtDW;

/* Real-time model */
RT_MODEL rtM_;
RT_MODEL *const rtM = &rtM_;
extern void gpio_in_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  const real_T *xD);
extern void gpio_in_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  boolean_T *gpio_in,
  real_T *xD);
extern void drs_actual_position_Outputs_wrapper(int32_T *actual_position,
  const real_T *xD);
extern void drs_actual_position_Update_wrapper(int32_T *actual_position,
  real_T *xD);
extern void can_tx_Outputs_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  const real_T *xD);
extern void can_tx_Update_wrapper(const boolean_T *trigger,
  const uint32_T *id,
  const uint32_T *dlc,
  const uint8_T *data,
  const uint32_T *channel,
  real_T *xD);
extern void can_rx_Outputs_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  const real_T *xD);
extern void can_rx_Update_wrapper(const uint32_T *id,
  const uint32_T *channel,
  boolean_T *received,
  uint8_T *data,
  real_T *xD);
extern void gpio_out_Outputs_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  const real_T *xD);
extern void gpio_out_Update_wrapper(const int32_T *port_popupvalue,
  const int32_T *pin_number,
  const boolean_T *gpio_out,
  real_T *xD);
extern void basic_pwm_Outputs_wrapper(const real_T *dutyCylce,
  const uint32_T *frequency,
  const real_T *xD);
extern void basic_pwm_Update_wrapper(const real_T *dutyCylce,
  const uint32_T *frequency,
  real_T *xD);

/* Model step function */
void model_step(void)
{
  real_T rtb_Sum_o;
  real_T rtb_Gain1;
  real_T rtb_Sum;
  real_T rtb_Saturate;
  real_T rtb_Saturation;
  int32_T i;

  /* DiscretePulseGenerator: '<Root>/Pulse Generator3' */
  rtDW.PulseGenerator3 = ((rtDW.clockTickCounter < 1) && (rtDW.clockTickCounter >=
    0));
  if (rtDW.clockTickCounter >= 99) {
    rtDW.clockTickCounter = 0;
  } else {
    rtDW.clockTickCounter++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator3' */

  /* S-Function (gpio_in): '<S8>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract,
    &rtDW.SFunctionBuilder_f, &rtDW.SFunctionBuilder_DSTATE);

  /* DiscreteTransferFcn: '<S17>/Discrete Transfer Fcn' */
  rtb_Sum = ((8.54490368397638E-7 * rtDW.DiscreteTransferFcn_states[0] +
              7.7731688642840433E-6 * rtDW.DiscreteTransferFcn_states[1]) +
             6.3152032519821958E-6 * rtDW.DiscreteTransferFcn_states[2]) +
    4.5573885542517735E-7 * rtDW.DiscreteTransferFcn_states[3];

  /* S-Function (drs_actual_position): '<S26>/S-Function Builder' */
  drs_actual_position_Outputs_wrapper( &rtDW.SFunctionBuilder,
    &rtDW.SFunctionBuilder_DSTATE_m);

  /* Gain: '<S16>/Gain1' incorporates:
   *  DataTypeConversion: '<S16>/Data Type Conversion'
   */
  rtb_Gain1 = 0.012586509028805263 * (real_T)rtDW.SFunctionBuilder;

  /* RelationalOperator: '<S5>/Compare' incorporates:
   *  Abs: '<Root>/Abs'
   *  Constant: '<S5>/Constant'
   *  Sum: '<Root>/Sum1'
   */
  rtDW.Compare = (fabs(rtb_Sum - rtb_Gain1) < 1.8849555921538759);

  /* S-Function (scanpack): '<Root>/CAN Pack6' */
  rtDW.CANPack6.ID = 392U;
  rtDW.CANPack6.Length = 1U;
  rtDW.CANPack6.Extended = 0U;
  rtDW.CANPack6.Remote = 0;
  rtDW.CANPack6.Data[0] = 0;
  rtDW.CANPack6.Data[1] = 0;
  rtDW.CANPack6.Data[2] = 0;
  rtDW.CANPack6.Data[3] = 0;
  rtDW.CANPack6.Data[4] = 0;
  rtDW.CANPack6.Data[5] = 0;
  rtDW.CANPack6.Data[6] = 0;
  rtDW.CANPack6.Data[7] = 0;

  {
    /* --------------- START Packing signal 0 ------------------
     *  startBit                = 0
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.SFunctionBuilder_f);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack6.Data[0] = rtDW.CANPack6.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<0);
              }
            }
          }
        }
      }
    }

    /* --------------- START Packing signal 1 ------------------
     *  startBit                = 1
     *  length                  = 1
     *  desiredSignalByteLayout = LITTLEENDIAN
     *  dataType                = UNSIGNED
     *  factor                  = 1.0
     *  offset                  = 0.0
     *  minimum                 = 0.0
     *  maximum                 = 0.0
     * -----------------------------------------------------------------------*/
    {
      {
        uint32_T packingValue = 0;

        {
          uint32_T result = (uint32_T) (rtDW.Compare);

          /* no scaling required */
          packingValue = result;
        }

        {
          {
            uint8_T packedValue;
            if (packingValue > (boolean_T)(1)) {
              packedValue = (uint8_T) 1;
            } else if (packingValue < (boolean_T)(0)) {
              packedValue = (uint8_T) 0;
            } else {
              packedValue = (uint8_T) (packingValue);
            }

            {
              {
                rtDW.CANPack6.Data[0] = rtDW.CANPack6.Data[0] | (uint8_T)
                  ((uint8_T)((uint8_T)(packedValue & (uint8_T)(((uint8_T)(1)) <<
                      0)) >> 0)<<1);
              }
            }
          }
        }
      }
    }
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack6' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack6' */
    if ((1 == rtDW.CANPack6.Length) && (rtDW.CANPack6.ID != INVALID_CAN_ID) ) {
      if ((392U == rtDW.CANPack6.ID) && (0U == rtDW.CANPack6.Extended) ) {
        (void) memcpy(&rtDW.MatrixConcatenate1[0], rtDW.CANPack6.Data,
                      1 * sizeof(uint8_T));
      }
    }
  }

  /* SignalConversion: '<Root>/ConcatBufferAtMatrix Concatenate1In2' */
  for (i = 0; i < 7; i++) {
    rtDW.MatrixConcatenate1[i + 1] = 0U;
  }

  /* End of SignalConversion: '<Root>/ConcatBufferAtMatrix Concatenate1In2' */

  /* S-Function (can_tx): '<S3>/can_tx' */
  can_tx_Outputs_wrapper(&rtDW.PulseGenerator3, &rtConstP.Constant_Value_pr,
    &rtConstP.pooled9, &rtDW.MatrixConcatenate1[0], &rtConstP.pooled9,
    &rtDW.can_tx_DSTATE);

  /* Sum: '<Root>/Sum' */
  rtb_Sum -= rtb_Gain1;

  /* Sum: '<S12>/Sum' incorporates:
   *  DiscreteIntegrator: '<S12>/Integrator'
   *  Gain: '<S12>/Proportional Gain'
   */
  rtb_Sum_o = 2.0 * rtb_Sum + rtDW.Integrator_DSTATE;

  /* Saturate: '<S12>/Saturate' */
  if (rtb_Sum_o > 10.0) {
    rtb_Saturate = 10.0;
  } else if (rtb_Sum_o < -10.0) {
    rtb_Saturate = -10.0;
  } else {
    rtb_Saturate = rtb_Sum_o;
  }

  /* End of Saturate: '<S12>/Saturate' */

  /* S-Function (gpio_in): '<S7>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled5, &rtConstB.Subtract_m,
    &rtDW.SFunctionBuilder_d, &rtDW.SFunctionBuilder_DSTATE_j);

  /* S-Function (can_rx): '<S1>/S-Function Builder' */
  can_rx_Outputs_wrapper(&rtConstP.Constant_Value_g, &rtConstP.pooled9,
    &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
    &rtDW.SFunctionBuilder_DSTATE_f);

  /* S-Function (scanpack): '<Root>/CAN Pack2' */
  rtDW.CANPack2.ID = 400U;
  rtDW.CANPack2.Length = 8U;
  rtDW.CANPack2.Extended = 0U;
  rtDW.CANPack2.Remote = 0;
  rtDW.CANPack2.Data[0] = 0;
  rtDW.CANPack2.Data[1] = 0;
  rtDW.CANPack2.Data[2] = 0;
  rtDW.CANPack2.Data[3] = 0;
  rtDW.CANPack2.Data[4] = 0;
  rtDW.CANPack2.Data[5] = 0;
  rtDW.CANPack2.Data[6] = 0;
  rtDW.CANPack2.Data[7] = 0;

  {
    (void) memcpy((rtDW.CANPack2.Data), &rtDW.SFunctionBuilder_o2[0],
                  8 * sizeof(uint8_T));
  }

  /* S-Function (scanunpack): '<Root>/CAN Unpack2' */
  {
    /* S-Function (scanunpack): '<Root>/CAN Unpack2' */
    if ((8 == rtDW.CANPack2.Length) && (rtDW.CANPack2.ID != INVALID_CAN_ID) ) {
      if ((400U == rtDW.CANPack2.ID) && (0U == rtDW.CANPack2.Extended) ) {
        {
          /* --------------- START Unpacking signal 0 ------------------
           *  startBit                = 4
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 1 ------------------
           *  startBit                = 0
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 2 ------------------
           *  startBit                = 8
           *  length                  = 8
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          {
            {
              real64_T outValue = 0;

              {
                {
                  uint8_T unpackedValue = 0;

                  {
                    uint8_T tempValue = (uint8_T) (0);

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 0))
                        >> 0)<<0);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 1))
                        >> 1)<<1);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 2))
                        >> 2)<<2);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 3))
                        >> 3)<<3);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 4))
                        >> 4)<<4);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 5))
                        >> 5)<<5);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 6))
                        >> 6)<<6);
                    }

                    {
                      tempValue = tempValue | (uint8_T)((uint8_T)((uint8_T)
                        ((rtDW.CANPack2.Data[1]) & (uint8_T)( (uint8_T) (1)<< 7))
                        >> 7)<<7);
                    }

                    unpackedValue = tempValue;
                  }

                  outValue = (real64_T) (unpackedValue);
                }
              }

              {
                real64_T result = (real64_T) outValue;
                rtDW.CANUnpack2_o3 = result;
              }
            }
          }

          /* --------------- START Unpacking signal 3 ------------------
           *  startBit                = 3
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 4 ------------------
           *  startBit                = 5
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 5 ------------------
           *  startBit                = 2
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */

          /* --------------- START Unpacking signal 6 ------------------
           *  startBit                = 1
           *  length                  = 1
           *  desiredSignalByteLayout = LITTLEENDIAN
           *  dataType                = UNSIGNED
           *  factor                  = 1.0
           *  offset                  = 0.0
           * -----------------------------------------------------------------------*/
          /*
           * Signal is not connected or connected to terminator.
           * No unpacking code generated.
           */
        }
      }
    }
  }

  /* Saturate: '<Root>/Saturation' */
  if (rtDW.CANUnpack2_o3 > 30.0) {
    rtb_Saturation = 30.0;
  } else if (rtDW.CANUnpack2_o3 < 0.0) {
    rtb_Saturation = 0.0;
  } else {
    rtb_Saturation = rtDW.CANUnpack2_o3;
  }

  /* End of Saturate: '<Root>/Saturation' */

  /* Chart: '<Root>/Chart' */
  if (rtDW.temporalCounter_i1 < 1023U) {
    rtDW.temporalCounter_i1++;
  }

  /* Gateway: Chart */
  /* During: Chart */
  if (rtDW.is_active_c3_model == 0U) {
    /* Entry: Chart */
    rtDW.is_active_c3_model = 1U;

    /* Entry Internal: Chart */
    /* Transition: '<S4>:14' */
    rtDW.is_c3_model = IN_startup;
    rtDW.temporalCounter_i1 = 0U;

    /* Entry 'startup': '<S4>:1' */
    rtDW.posSollOUT = 0.0;
    rtDW.speed = 3.0;
    rtDW.posNull = 0.0;
  } else {
    switch (rtDW.is_c3_model) {
     case IN_home:
      /* During 'home': '<S4>:24' */
      if (rtDW.temporalCounter_i1 >= 1000U) {
        /* Transition: '<S4>:25' */
        rtDW.is_c3_model = IN_operational;

        /* Entry 'operational': '<S4>:16' */
        rtDW.speed = 1.0;
        rtDW.posSollOUT = rtb_Saturation;
      }
      break;

     case IN_homing:
      /* During 'homing': '<S4>:3' */
      if (rtDW.SFunctionBuilder_d) {
        /* Transition: '<S4>:23' */
        rtDW.is_c3_model = IN_save;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'save': '<S4>:27' */
        rtDW.speed = 10.0;
        rtDW.posSollOUT = 0.0;
        rtDW.posNull = rtb_Gain1 + 0.31415926535897931;
      } else {
        /* Transition: '<S4>:22' */
        rtDW.is_c3_model = IN_homing;

        /* Entry 'homing': '<S4>:3' */
        rtDW.speed = 4.0;
        rtDW.posSollOUT = -43.982297150257104;
      }
      break;

     case IN_operational:
      /* During 'operational': '<S4>:16' */
      /* Transition: '<S4>:31' */
      rtDW.is_c3_model = IN_operational;

      /* Entry 'operational': '<S4>:16' */
      rtDW.speed = 1.0;
      rtDW.posSollOUT = rtb_Saturation;
      break;

     case IN_save:
      /* During 'save': '<S4>:27' */
      if (rtDW.temporalCounter_i1 >= 500U) {
        /* Transition: '<S4>:28' */
        rtDW.is_c3_model = IN_home;
        rtDW.temporalCounter_i1 = 0U;

        /* Entry 'home': '<S4>:24' */
        rtDW.speed = 2.0;
        rtDW.posSollOUT = 0.0;
      }
      break;

     default:
      /* During 'startup': '<S4>:1' */
      if (rtDW.temporalCounter_i1 >= 1000U) {
        /* Transition: '<S4>:4' */
        rtDW.is_c3_model = IN_homing;

        /* Entry 'homing': '<S4>:3' */
        rtDW.speed = 4.0;
        rtDW.posSollOUT = -43.982297150257104;
      }
      break;
    }
  }

  /* End of Chart: '<Root>/Chart' */

  /* Switch: '<Root>/Switch' incorporates:
   *  Constant: '<Root>/Constant'
   *  Constant: '<Root>/Constant3'
   *  Product: '<Root>/Divide'
   *  Product: '<Root>/Product'
   */
  if (rtDW.Compare) {
    rtb_Gain1 = 0.0;
  } else {
    rtb_Gain1 = rtb_Saturate / (14.0 * rtDW.speed);
  }

  /* End of Switch: '<Root>/Switch' */

  /* RelationalOperator: '<S18>/Compare' incorporates:
   *  Constant: '<S18>/Constant'
   */
  rtDW.Compare_k = (rtb_Gain1 < 0.0);

  /* S-Function (gpio_out): '<S20>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled5, &rtConstB.Subtract_l,
    &rtDW.Compare_k, &rtDW.SFunctionBuilder_DSTATE_a);

  /* RelationalOperator: '<S19>/Compare' incorporates:
   *  Constant: '<S19>/Constant'
   */
  rtDW.Compare_h = (rtb_Gain1 > 0.01);

  /* S-Function (gpio_out): '<S21>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_a,
    &rtDW.Compare_h, &rtDW.SFunctionBuilder_DSTATE_e);

  /* S-Function (gpio_in): '<S23>/S-Function Builder' */
  gpio_in_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_mv,
    &rtDW.SFunctionBuilder_a, &rtDW.SFunctionBuilder_DSTATE_i);

  /* Switch: '<S6>/Switch' incorporates:
   *  Logic: '<S6>/Logical Operator5'
   */
  if (!rtDW.SFunctionBuilder_a) {
    /* Abs: '<S6>/Abs' */
    rtb_Gain1 = fabs(rtb_Gain1);

    /* Saturate: '<S6>/Saturation1' */
    if (rtb_Gain1 > 1.0) {
      /* Saturate: '<S6>/Saturation' */
      rtDW.Saturation = 1.0;
    } else {
      /* Saturate: '<S6>/Saturation' */
      rtDW.Saturation = rtb_Gain1;
    }

    /* End of Saturate: '<S6>/Saturation1' */
  } else {
    /* Saturate: '<S6>/Saturation' incorporates:
     *  Constant: '<S6>/error value'
     */
    rtDW.Saturation = 0.0;
  }

  /* End of Switch: '<S6>/Switch' */

  /* S-Function (basic_pwm): '<S22>/S-Function Builder' */
  basic_pwm_Outputs_wrapper(&rtDW.Saturation, &rtConstP.Constant_Value_n3,
    &rtDW.SFunctionBuilder_DSTATE_mz);

  /* Logic: '<S6>/Logical Operator2' */
  rtDW.LogicalOperator2 = !rtDW.Compare_k;

  /* S-Function (gpio_out): '<S24>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_i,
    &rtDW.LogicalOperator2, &rtDW.SFunctionBuilder_DSTATE_i3);

  /* Logic: '<S6>/Logical Operator3' */
  rtDW.LogicalOperator3 = !rtDW.Compare_h;

  /* S-Function (gpio_out): '<S25>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_h,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_mi);

  /* Logic: '<Root>/Logical Operator' */
  rtDW.LogicalOperator = !rtDW.SFunctionBuilder_f;

  /* S-Function (gpio_out): '<S9>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_f,
    &rtDW.LogicalOperator, &rtDW.SFunctionBuilder_DSTATE_n);

  /* S-Function (gpio_out): '<S11>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_mr,
    &rtConstP.pooled10, &rtDW.SFunctionBuilder_DSTATE_g);

  /* S-Function (gpio_out): '<S13>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_o,
    &rtConstP.pooled10, &rtDW.SFunctionBuilder_DSTATE_l);

  /* S-Function (gpio_out): '<S14>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_fe,
    &rtConstP.pooled11, &rtDW.SFunctionBuilder_DSTATE_ih);

  /* DiscretePulseGenerator: '<Root>/Pulse Generator2' */
  rtDW.PulseGenerator2 = ((rtDW.clockTickCounter_j < 250) &&
    (rtDW.clockTickCounter_j >= 0));
  if (rtDW.clockTickCounter_j >= 499) {
    rtDW.clockTickCounter_j = 0;
  } else {
    rtDW.clockTickCounter_j++;
  }

  /* End of DiscretePulseGenerator: '<Root>/Pulse Generator2' */

  /* S-Function (gpio_out): '<S10>/S-Function Builder' */
  gpio_out_Outputs_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_g,
    &rtDW.PulseGenerator2, &rtDW.SFunctionBuilder_DSTATE_gi);

  /* S-Function "gpio_in_wrapper" Block: <S8>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract,
    &rtDW.SFunctionBuilder_f, &rtDW.SFunctionBuilder_DSTATE);

  /* Update for DiscreteTransferFcn: '<S17>/Discrete Transfer Fcn' incorporates:
   *  DataStoreRead: '<Root>/Data Store Read'
   *  Sum: '<Root>/Sum2'
   */
  rtb_Saturation = ((((rtDW.posSollOUT + rtDW.posNull) - -3.3103548194736128 *
                      rtDW.DiscreteTransferFcn_states[0]) - 3.9725119459305933 *
                     rtDW.DiscreteTransferFcn_states[1]) - -2.0120794769667949 *
                    rtDW.DiscreteTransferFcn_states[2]) - 0.34993774911115444 *
    rtDW.DiscreteTransferFcn_states[3];
  rtDW.DiscreteTransferFcn_states[3] = rtDW.DiscreteTransferFcn_states[2];
  rtDW.DiscreteTransferFcn_states[2] = rtDW.DiscreteTransferFcn_states[1];
  rtDW.DiscreteTransferFcn_states[1] = rtDW.DiscreteTransferFcn_states[0];
  rtDW.DiscreteTransferFcn_states[0] = rtb_Saturation;

  /* S-Function "drs_actual_position_wrapper" Block: <S26>/S-Function Builder */
  drs_actual_position_Update_wrapper( &rtDW.SFunctionBuilder,
    &rtDW.SFunctionBuilder_DSTATE_m);

  /* S-Function "can_tx_wrapper" Block: <S3>/can_tx */
  can_tx_Update_wrapper(&rtDW.PulseGenerator3, &rtConstP.Constant_Value_pr,
                        &rtConstP.pooled9, &rtDW.MatrixConcatenate1[0],
                        &rtConstP.pooled9, &rtDW.can_tx_DSTATE);

  /* Update for DiscreteIntegrator: '<S12>/Integrator' incorporates:
   *  Gain: '<S12>/Integral Gain'
   *  Sum: '<S12>/SumI1'
   *  Sum: '<S12>/SumI2'
   */
  rtDW.Integrator_DSTATE += (0.323 * rtb_Sum + (rtb_Saturate - rtb_Sum_o)) *
    0.001;

  /* S-Function "gpio_in_wrapper" Block: <S7>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled5, &rtConstB.Subtract_m,
    &rtDW.SFunctionBuilder_d, &rtDW.SFunctionBuilder_DSTATE_j);

  /* S-Function "can_rx_wrapper" Block: <S1>/S-Function Builder */
  can_rx_Update_wrapper(&rtConstP.Constant_Value_g, &rtConstP.pooled9,
                        &rtDW.SFunctionBuilder_o1, &rtDW.SFunctionBuilder_o2[0],
                        &rtDW.SFunctionBuilder_DSTATE_f);

  /* S-Function "gpio_out_wrapper" Block: <S20>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled5, &rtConstB.Subtract_l,
    &rtDW.Compare_k, &rtDW.SFunctionBuilder_DSTATE_a);

  /* S-Function "gpio_out_wrapper" Block: <S21>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_a,
    &rtDW.Compare_h, &rtDW.SFunctionBuilder_DSTATE_e);

  /* S-Function "gpio_in_wrapper" Block: <S23>/S-Function Builder */
  gpio_in_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_mv,
    &rtDW.SFunctionBuilder_a, &rtDW.SFunctionBuilder_DSTATE_i);

  /* S-Function "basic_pwm_wrapper" Block: <S22>/S-Function Builder */
  basic_pwm_Update_wrapper(&rtDW.Saturation, &rtConstP.Constant_Value_n3,
    &rtDW.SFunctionBuilder_DSTATE_mz);

  /* S-Function "gpio_out_wrapper" Block: <S24>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_i,
    &rtDW.LogicalOperator2, &rtDW.SFunctionBuilder_DSTATE_i3);

  /* S-Function "gpio_out_wrapper" Block: <S25>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_h,
    &rtDW.LogicalOperator3, &rtDW.SFunctionBuilder_DSTATE_mi);

  /* S-Function "gpio_out_wrapper" Block: <S9>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_f,
    &rtDW.LogicalOperator, &rtDW.SFunctionBuilder_DSTATE_n);

  /* S-Function "gpio_out_wrapper" Block: <S11>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_mr,
    &rtConstP.pooled10, &rtDW.SFunctionBuilder_DSTATE_g);

  /* S-Function "gpio_out_wrapper" Block: <S13>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_o,
    &rtConstP.pooled10, &rtDW.SFunctionBuilder_DSTATE_l);

  /* S-Function "gpio_out_wrapper" Block: <S14>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled3, &rtConstB.Subtract_fe,
    &rtConstP.pooled11, &rtDW.SFunctionBuilder_DSTATE_ih);

  /* S-Function "gpio_out_wrapper" Block: <S10>/S-Function Builder */
  gpio_out_Update_wrapper(&rtConstP.pooled7, &rtConstB.Subtract_g,
    &rtDW.PulseGenerator2, &rtDW.SFunctionBuilder_DSTATE_gi);
}

/* Model initialize function */
void model_initialize(void)
{
  /*-----------S-Function Block: <Root>/CAN Unpack6 -----------------*/

  /*-----------S-Function Block: <Root>/CAN Unpack2 -----------------*/

  /* S-Function Block: <S8>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S26>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_m = initVector[0];
      }
    }
  }

  /* S-Function Block: <S3>/can_tx */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.can_tx_DSTATE = initVector[0];
      }
    }
  }

  /* S-Function Block: <S7>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_j = initVector[0];
      }
    }
  }

  /* S-Function Block: <S1>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_f = initVector[0];
      }
    }
  }

  /* S-Function Block: <S20>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_a = initVector[0];
      }
    }
  }

  /* S-Function Block: <S21>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_e = initVector[0];
      }
    }
  }

  /* S-Function Block: <S23>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_i = initVector[0];
      }
    }
  }

  /* S-Function Block: <S22>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_mz = initVector[0];
      }
    }
  }

  /* S-Function Block: <S24>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_i3 = initVector[0];
      }
    }
  }

  /* S-Function Block: <S25>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_mi = initVector[0];
      }
    }
  }

  /* S-Function Block: <S9>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_n = initVector[0];
      }
    }
  }

  /* S-Function Block: <S11>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_g = initVector[0];
      }
    }
  }

  /* S-Function Block: <S13>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_l = initVector[0];
      }
    }
  }

  /* S-Function Block: <S14>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_ih = initVector[0];
      }
    }
  }

  /* S-Function Block: <S10>/S-Function Builder */
  {
    real_T initVector[1] = { 0 };

    {
      int_T i1;
      for (i1=0; i1 < 1; i1++) {
        rtDW.SFunctionBuilder_DSTATE_gi = initVector[0];
      }
    }
  }
}

/*
 * File trailer for generated code.
 *
 * [EOF]
 */
